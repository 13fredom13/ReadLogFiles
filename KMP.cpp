#include "KMP.h"

KMP::KMP() :    
                mMask(NULL), 
                mMaskSize(0), 
                mStr(NULL),
                mStrSize(0),
                mStrIndex(0),
                mMaskIndex(0),
                mFoundRowIndex(0),
                mContinueSearch(false), 
                mAfterStar(false),
                mFail( false ),
                mPrefixKMP(NULL)
{
    memset(mPrefixKMPSSO, 0, (MASK_SSO_SIZE) * sizeof(size_t));
    mResult.number = 0;
    mResult.type = 0;
}

KMP::~KMP()
{
    // if SSO has not been used and the pointer is valid - free the memory
    if (!mPrefixKMPSSO[USED_STACK] && mPrefixKMP)
    {
        delete[] mPrefixKMP;
        mPrefixKMP = NULL;
    }
}

bool KMP::setFilter(const char* mask)
{
    // if SSO has not been used and the pointer is valid - free the memory
    if (!mPrefixKMPSSO[USED_STACK] && mPrefixKMP)
    {
        delete[] mPrefixKMP;
        mPrefixKMP = NULL;
    }

    mMaskSize = strlen(mask);
    if (!mMaskSize)
    {
        ERR("Filter size is zero");
        return false;    
    }
    
    for (size_t i = 0; i < mMaskSize; i++)
    {
        if (mask[i] == '\n' || mask[i] == '\r')
        {
            ERR("Filter uses forbidden characters");
            return false;
        }
    }

    mMask = mask;

    // Small String Optimization
    if (mMaskSize < MASK_SSO_SIZE)
    {
        mPrefixKMP = mPrefixKMPSSO + 1;
        mPrefixKMPSSO[USED_STACK] = 1; // use SSO
    }
    else
    {
        mPrefixKMP = new (std::nothrow) size_t[mMaskSize];
        if (!mPrefixKMP)
        {
            ERR("Failed to allocate memory for prefix table");
            return false;
        }

        mPrefixKMPSSO[USED_STACK] = 0; // do not use SSO
    }

    resetSearchState();
    createKMPPrefix();

    return true;
}

// create a prefix table. Knuth�Morris�Pratt algorithm
void KMP::createKMPPrefix()
{
    size_t beforeLeftStar = 0;
    for (size_t leftStar = 0, afterRightStar = 0; leftStar < mMaskSize; ++leftStar)
    {
        if (mMask[leftStar] == STAR)
        {
            // looking for the first char after the star
            afterRightStar = leftStar;
            while (afterRightStar < mMaskSize && mMask[afterRightStar] == STAR)
                ++afterRightStar;

            // stars put the index on the next char after them
            for (size_t i = leftStar; i < afterRightStar; ++i)
                mPrefixKMP[i] = afterRightStar;

            // fill the mask for the line in front of the star
            if (beforeLeftStar < leftStar)
                createKMPPrefixImpl(beforeLeftStar, leftStar - 1);

            beforeLeftStar = afterRightStar;
            leftStar = afterRightStar;
        }
    }

    // fill the mask for the line after the last star
    if (beforeLeftStar < mMaskSize)
        createKMPPrefixImpl(beforeLeftStar, mMaskSize - 1);
}

void KMP::createKMPPrefixImpl(size_t l, size_t r)
{
    size_t count = (r + 1) - l ;
    mPrefixKMP[l] = 0;

    for (size_t i = 1, j = 0; i < count; i++)
    {
        // if the characters are different (including not equal QUESTIONMARK by condition), then reduce the length j
        while (j > 0 && (mMask[j + l] != mMask[i + l] && mMask[i + l] != QUESTIONMARK && mMask[j + l] != QUESTIONMARK))
            j = mPrefixKMP[(j + l) - 1 ];

        // if the characters match (including equal QUESTIONMARK by condition), then increase the length j
        if (mMask[j + l] == mMask[i + l] || mMask[j + l] == QUESTIONMARK || mMask[i + l] == QUESTIONMARK)
            j++;

        mPrefixKMP[i + l] = j;
    }

    // for 2 and subsequent subsets I set the offset so as not to go beyond the sets. Subsets are separated by STAR
    if (l) 
    {
        for (size_t i = l; i <= r; ++i)
            mPrefixKMP[i] += l;
    }
}

ResultPosition KMP::GetNextLineKMP(const char * str, size_t strSize)
{
    mStr = str;
    mStrSize = strSize;

    if (!mPrefixKMP)
    {
        ERR("Missing KMP prefix");
        mResult.type = EPIC_FAIL;
        return mResult;
    }

    if (!mStr || !mStrSize)
    {
        ERR("Missing string");
        mResult.type = EPIC_FAIL;
        return mResult;
    }

    // by mask index shift, the continuation of the search is understood. If successful, the flag SUCCESS_PREVIOUS_ROW will be sent
    for (mContinueSearch = (mMaskIndex ? true : false); mStrIndex < mStrSize; mStrIndex++)
    {
        // if the end of line character is found, the "search state" is reset and a new line is crossed
        if (checkEndLine())
            continue;

        switch (mMask[mMaskIndex])
        {
        case STAR:
        {
            // if there are only stars left
            if (mPrefixKMP[mMaskIndex] == mMaskSize)
                return returnSuccess();

            // go to the next letter after the star, set the star flag, scroll the current char in the second circle
            mMaskIndex = mPrefixKMP[mMaskIndex];
            mAfterStar = true;
            --mStrIndex;

            break;
        }
        case QUESTIONMARK:
        {
            if (mMaskIndex + 1 == mMaskSize) // if the mask is over:
            {
                if (mStrIndex > 0 && mMask[mMaskIndex - 1] == STAR) // optimization
                    return returnSuccess();

                if (mStrIndex + 1 == mStrSize || mStr[mStrIndex + 1] == '\n' || mStr[mStrIndex + 1] == '\r') // and the line is over
                    return returnSuccess();
                else
                    shiftToLeftPrefixKMP(); // but the line does not end, move the mask back to the prefix

                break;
            }
            else if (mPrefixKMP[mMaskIndex + 1] == mMaskSize) // if the mask does not end, but there are only stars
            {
                return returnSuccess();
            }

            ++mMaskIndex;
            break;
        }
        default:
        {
            if (mMask[mMaskIndex] == mStr[mStrIndex])
            {
                if (mMaskIndex + 1 == mMaskSize) // if the mask is over
                {
                    if (mStrIndex + 1 == mStrSize || mStr[mStrIndex + 1] == '\n' || mStr[mStrIndex + 1] == '\r') // and the line is over
                        return returnSuccess();
                    else 
                        shiftToLeftPrefixKMP(); // but the line does not end, I move the mask back to the prefix or go to a new line

                    break;
                }
                else if (mPrefixKMP[mMaskIndex + 1] == mMaskSize) // if the mask does not end, but there are only stars
                {
                    return returnSuccess();
                }

                ++mMaskIndex;
                break;
            }
            else // mask is not equal to character
            {
                // instead of calling shiftToLeftPrefixKMP (profiler)
                
                // if there was no star, skip this line
                if (!mAfterStar) 
                {
                    badCurStrShiftToNewLine();
                    break;
                }

                // move the mask back to the prefix or go to a new line
                do 
                {
                    mMaskIndex = mPrefixKMP[mMaskIndex - 1];
                } 
                while (mMask[mMaskIndex] != mStr[mStrIndex] && mMaskIndex != mPrefixKMP[mMaskIndex - 1] && mMask[mMaskIndex] != QUESTIONMARK);

                // if there is a match and this is not the end of the mask, then I take the next mask element for a new iteration
                if ((mMask[mMaskIndex] == mStr[mStrIndex] || mMask[mMaskIndex] == QUESTIONMARK) && (mMaskIndex + 1) != mMaskSize)
                    ++mMaskIndex;

                break;
            }
        }
        }
    }

    return returnFail();
}

bool KMP::checkEndLine()
{
    if (mStr[mStrIndex] == '\n' || mStr[mStrIndex] == '\r')
    {
        badCurStrShiftToNewLine();
        return true;
    }

    return false;
}

// if there was no match or the mask ended and the line is not
void KMP::shiftToLeftPrefixKMP()
{
    // if there was no star, skip this line
    if (!mAfterStar)
    {
        badCurStrShiftToNewLine();
        return;
    }

    // move the mask back to the prefix or go to a new line
    do
    {
        mMaskIndex = mPrefixKMP[mMaskIndex - 1];
    }
    while (mMask[mMaskIndex] != mStr[mStrIndex] && mMaskIndex != mPrefixKMP[mMaskIndex - 1] && mMask[mMaskIndex] != QUESTIONMARK);

    // if there is a match and this is not the end of the mask, then I take the next mask element for a new iteration
    if ((mMask[mMaskIndex] == mStr[mStrIndex] || mMask[mMaskIndex] == QUESTIONMARK) && (mMaskIndex + 1) != mMaskSize)
        ++mMaskIndex;
}

// if the line does not fit, move to the next
void KMP::badCurStrShiftToNewLine()
{
    mMaskIndex = 0;
    mContinueSearch = false;
    mAfterStar = false;

    // looking for '\n' or '\r':
    while (mStrIndex < mStrSize && mStr[mStrIndex] != '\n' && mStr[mStrIndex] != '\r')
        ++mStrIndex;

    // if '\n' or '\r' is not found => I will search in the next block
    if (mStrIndex == mStrSize) 
    {
        mFail = true;
        return;
    }

    // looking for char after \n or \r\n
    while (mStrIndex < mStrSize && (mStr[mStrIndex] == '\n' || mStr[mStrIndex] == '\r'))
        ++mStrIndex;

    // if char is not found => I will search in the next block
    if (mStrIndex == mStrSize)
    {
        mFail = true;
        return;
    }

    // if char found:
    mFoundRowIndex = mStrIndex; // I fix the position and start looking for a mask with this char
    mStrIndex--; // start looking for a mask with this char
}

void KMP::resetSearchState()
{
    mAfterStar = false;
    mStrIndex = 0;
    mMaskIndex = 0;
    mFoundRowIndex = 0;
    mFail = false;
}

ResultPosition KMP::returnSuccess()
{
    if (mContinueSearch) // success of the previous block
    {
        assert(!mFoundRowIndex); // because we continued the search
        mResult.type = SUCCESS_PREVIOUS_ROW;
        mResult.number = mFoundRowIndex;
    }
    else // success in the current block
    {
        mResult.type = SUCCESS;
        mResult.number = mFoundRowIndex;
    }
    
    resetSearchState();
    return mResult;
}

ResultPosition KMP::returnFail()
{
    if (mFail)
    {
        mResult.type = FAIL;
        mResult.number = 0;
        resetSearchState();
    }
    else // possible success (there were matches, but the line ended, but the mask did not). We do not reset the condition!
    {
        mResult.type = STILL_SUCCESS_WAIT_NEXT_BLOCK;
        mResult.number = mFoundRowIndex;

        mFoundRowIndex = 0;
        mStrIndex = 0;
    }
    return mResult;
}
