#include "TestCLogReader.h"

#ifdef RUN_TEST

// Set a different data block size for reading from a file
const size_t INITIAL_FILE_BLOCK_SIZE = 0;
const size_t DELTA_FILE_BLOCK = 50000;
const size_t MAX_FILE_BLOCK_SIZE = 150000;

// Set a different data block size for the result
const int FOUND_STRING_MIN_SIZE = 20;
const int FOUND_STRING_STEP_SIZE = 20;
const int FOUND_STRING_MAX_SIZE = 60;


bool TestCLogReader::runTestFileBlockSize(const char * filename, const char * mask)
{
    printf("\n\n\n----Test of different size of a block of reading from a file----\n");
    CLogReader * logReader = new CLogReader(INITIAL_FILE_BLOCK_SIZE);
    std::map<size_t, std::set<size_t>> mResult;

    std::map<clock_t, size_t> resTimeMap;
    clock_t workTime = clock();
    ptrdiff_t allTime = 0;

    for (size_t blockSize = INITIAL_FILE_BLOCK_SIZE; blockSize <= MAX_FILE_BLOCK_SIZE; blockSize += DELTA_FILE_BLOCK)
    {
        delete logReader;
        logReader = new CLogReader(blockSize); // set different block size

        if (!logReader->Open(filename))
        {
            delete logReader;
            ERR("Cannot open file\n");
            return false;
        }

        if (!logReader->SetFilter(mask))
        {
            delete logReader;
            ERR("Cannot set filter");
            return false;
        }

        char* foundString = new char[FOUND_STRING_SIZE];

        int countFound = 0;
        workTime = clock();
        while (logReader->GetNextLine(foundString, FOUND_STRING_SIZE))
            ++countFound;
        workTime = clock() - workTime;

        // save time
        resTimeMap.insert(std::pair<clock_t, size_t>{workTime, blockSize});
        allTime += workTime;
        mResult[countFound].insert(blockSize);
    }

    if (mResult.size() == 1) // if all tests found an equal number of rows
    {
        printf("[OK] : all tests found = %zu strings\n", mResult.begin()->first);
        printf("[OK] : all read block size : ");

        for (const auto& elem : mResult.begin()->second)
        {
            printf("%zu; ", elem);
        }
        printf("\n");

        printTimeTest(resTimeMap, allTime);

        delete logReader;
        return true;
    }

    for (const auto& elem : mResult) // if the tests found a different number of lines
    {
        printf("[ERR] : number of found = %zu block size = ", elem.first);
        for (const auto& size : elem.second)
            printf("%zu; ", size);

        printf("\n");
    }

    printTimeTest(resTimeMap, allTime);

    delete logReader;
    return false;
}


void TestCLogReader::printTimeTest(std::map<clock_t, size_t>& resTimeMap, size_t allTime)
{
    printf("\nTime:\n");
    for (std::map<clock_t, size_t>::reverse_iterator it = resTimeMap.rbegin(); it != resTimeMap.rend(); ++it)
        printf("block size = %zu time = %d seconds\n", it->second, it->first);
}


bool TestCLogReader::runTestResultBlockSize(const char* filename, const char* mask)
{
    printf("\n\n\n----Test of different size of result record----\n");
    CLogReader* logReader = new CLogReader();
    srand(time(NULL));

    std::map<size_t, std::set<size_t>> mResult;
    
    std::map<clock_t, size_t> resTimeMap;
    clock_t workTime = clock() / CLK_TCK;
    ptrdiff_t allTime = 0;

    for (int foundStringSize = FOUND_STRING_MIN_SIZE; foundStringSize <= FOUND_STRING_MAX_SIZE; foundStringSize += FOUND_STRING_STEP_SIZE)
    {
        if (rand() % 2) // correctness test for random continuation of work with the same object
        {
            delete logReader;
            logReader = new CLogReader();
        }

        if (!logReader->Open(filename))
        {
            delete logReader;
            ERR("Cannot open file\n");
            return false;
        }

        if (!logReader->SetFilter(mask))
        {
            delete logReader;
            ERR("Cannot set filter");
            return false;
        }

        char* foundString = new char[foundStringSize];

        int numberFound = 0;
        workTime = clock();
        while (logReader->GetNextLine(foundString, foundStringSize))
            ++numberFound;
        workTime = clock() - workTime;

        // save time
        resTimeMap[workTime] = foundStringSize;
        allTime += workTime;

        mResult[numberFound].insert(foundStringSize);

        if (rand() % 2) // test of correctness of additional closure
        {
            logReader->Close();
        }

        delete[] foundString;
    }

    if (mResult.size() == 1) // if all tests found an equal number of rows
    {
        printf("[OK] : all tests found = %zu strings\n", mResult.begin()->first);
        printf("[OK] : all read block size : ");

        for (const auto& elem : mResult.begin()->second)
        {
            printf("%zu; ", elem);
        }
        printf("\n");

        printTimeTest(resTimeMap, allTime);

        delete logReader;
        return true;
    }


    for (const auto& elem : mResult) // if the tests found a different number of lines
    {
        printf("[ERR] : number of found = %zu block size = ", elem.first);
        for (const auto& size : elem.second)
            printf("%zu; ", size);

        printf("\n");
    }

    printTimeTest(resTimeMap, allTime);

    delete logReader;
    return false;
}

#endif