#pragma once
#ifndef TESTCLOGREADER_H
#define TESTCLOGREADER_H

#include "Core.h"
#include "KMP.h"
#include "CLogReader.h"

#include <cstdlib>
#include <ctime>
#include <map>
#include <set>

class TestCLogReader
{
public:
    /*
        The function starts the project N times. Each time changes the size of the block that reads from the file. Compares results and lead time.
    */
    static bool runTestFileBlockSize(const char* filename, const char* mask);


    /*
        The function starts the project N times. Each time changes the number of bytes that must be returned when the string matches the mask. Compares results and lead time.
    */
    static bool runTestResultBlockSize(const char* filename, const char* mask);

private:
    /*
        Helper function to display runtime on screen.
    */
    static void printTimeTest(std::map<clock_t, size_t>& resTimeMap, size_t allTime);
};

#endif