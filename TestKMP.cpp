#include "TestKMP.h"

#ifdef RUN_TEST
#include <cstdlib>
#include <ctime>
#include <map>

#define RUN_TIME_TEST
const size_t REPEAR_TEST_FOR_MEASURE_TIME = 30000;

const char* arrayTest[][3] =
{
    { OK,  "?",		"e" },
    { NOK, "?",		"vo" },
    { OK,  "*",		"vo" },
    { NOK, " ",		"vo" },
    { NOK, " ",		"ewq" },
    { OK,  " ",		" " },
    { NOK, "*aaa",		"caabaa" },
    { OK,  "vova*?*",		"vovaeeeee3eeeee" },
    { OK,  "vova ?vova",		"vova 1vova" },
    { OK,  "vova? vova",		"vova1 vova" },
    { OK,  "?vova vova",		"1vova vova" },
    { NOK, "*vova vovadsassssdcds cdsc dsc dsc dsc dsc sdc ds",		"dsadasdsvova vova" },
    { OK,  "*jnjnjnjnjnjnjnjnjnjnjnjnjnj*",		"�HM��6�D<�?�6�D<�?�PK* * ;A�� skfjds fkjdsnfkjds fkjdsnnjnjnjnjnjnjnjnjnjnjnjnjnjnj" },
    { OK,  "*jnjnjnjnjnjnjnjnjnjnjnjnjnj*",		"* ;A�� skfjds fkjdsnfkjds fkjdsnnjnjnjnjnjnjnjnjnjnjnjnjnjnj" },
    { OK,  "*vova vova",		"dsadasdsvova vova" },
    { OK,  "vova vova",		"vova vova" },
    { OK,  "*?vova vova?",		"1vova vova1" },
    { OK,  "?*vova vova?",		"1vova vova1" },
    { OK,  "?v*ova vova?",		"1vova vova1" },
    { OK,  "?vo*va vova?",		"1vova vova1" },
    { OK,  "?vov*a vova?",		"1vova vova1" },
    { OK,  "?vova* vova?",		"1vova vova1" },
    { OK,  "?vova *vova?",		"1vova vova1" },
    { OK,  "?vova v*ova?",		"1vova vova1" },
    { OK,  "?vova vo*va?",		"1vova vova1" },
    { OK,  "?vova vov*a?",		"1vova vova1" },
    { OK,  "?vova vova*?",		"1vova vova1" },
    { OK,  "?vova vova",		"1vova vova" },
    { OK,  "v?ova vova",		"v1ova vova" },
    { OK,  "vo?va vova",		"vo1va vova" },
    { OK,  "vov?a vova",		"vov1a vova" },
    { OK,  "vova? vova",		"vova1 vova" },
    { OK,  "vova ?vova",		"vova 1vova" },
    { OK,  "vova v?ova",		"vova v1ova" },
    { OK,  "vova vo?va",		"vova vo1va" },
    { OK,  "vova vov?a",		"vova vov1a" },
    { OK,  "vova vova?",		"vova vova1" },
    { OK,  "????*********vova vova",		"1111gsdgasdgasdgasdgasdgasdgvova vova" },
    { OK,  "*vovo vovo",		"sdsadsadsa asdasd dsa*dD)(SAD)(SA*D 0vovovovo vovo" },
    { OK,  "*vovo vovo",		"sdsadsadsa asdasd dsa*dD)(SAD)(SA*D 0vovovovo vovo" },
    { OK,  "*vovo vovo",		"sdsadsadsa asdasd dsa*dD)(SAD)(SA*D 0vovovovo vovo" },
    { OK,  "*vovo vovo",		"sdsadsadsa asdasd dsa*dD)(SAD)(SA*D 0vovovovo vovo" },
    { OK,  "*vovo vovo",		"sdsadsadsa asdasd dsa*dD)(SAD)(SA*D 0vovovovo vovo" },
    { OK,  "*vovo vovo",		"sdsadsadsa asdasd dsa*dD)(SAD)(SA*D 0vovovovo vovo" },
    { NOK, "vova vova",		"vovavova" },
    { NOK, "?vova vova",		"vova vova" },
    { NOK, "v?ova vova",		"vova vova" },
    { NOK, "vo?va vova",		"vova vova" },
    { NOK, "vov?a vova",		"vova vova" },
    { NOK, "vova? vova",		"vova vova" },
    { NOK, "vova ?vova",		"vova vova" },
    { NOK, "vova v?ova",		"vova vova" },
    { NOK, "vova vo?va",		"vova vova" },
    { NOK, "vova vov?a",		"vova vova" },
    { NOK, "vova vova?",		"vova vova" },
    { OK,  "vova vova",		"vova\nvova vova" },
    { OK,  "vova vova",		"vova vova\nvovvvv" },
    { NOK, "vova vova",		"vova\0vova" },
    { NOK, "vova vova",		"vova\rvova" },
    { NOK, "vova vova",		"vova\nvova" },
    { NOK, "vova vova",		"vova\bvova" },
    { NOK, "vova vova",		"vova?vova" },
    { NOK, "vova vova",		"vova*vova" },
    { OK,  "vova vova?",		"vova vova1" },
    { OK,  "vova vova",		"vova vova" },
    { OK,  "vova vova",		"vova vova" },
    { OK,  "vova vova",		"vova vova" },
    { NOK, "*vova",		"dsadsadvova3" },
    { OK,  "*vova",		"dsadasdsvova" },
    { OK,  "*vova",		"23213vova" },
    { NOK, "*vova",		"cdscvova3" },
    { NOK, "*aaa",		"caabaa" },
    { NOK, "*aaaa",		"caaabaaa" },
    { OK,  "*a",		"caba" },
    { NOK, "*popo",		"popbpoppop" },
    { OK,  "*popo",		"popbpoppopo" },
    { OK,  "*popo",		"popbpopo" },
    { NOK, "*popo",		"popbpop" },
    { OK,  "*aaa",		"caabaaa" },
    { OK,  "*v",		"\n v" },
    { OK,  "*v",		"vv\n\r\tv" },
    { OK,  "*vova",		"vovova" },
    { OK,  "*vova",		"vovova\n" },
    { OK,  "*aaa",		"aabaaa" },
    { NOK, "*vova",		"vovova " },
    { OK,  "vova*?",		"vova1222" },
    { OK,  "vova*?",		"vova1" },
    { OK,  "*vova",		"vovova" },
    { NOK, "vova",		"vovova" },
    { OK,  "*vova",		"vovovovovovovovovovovovova" },
    { NOK, "*vova",		"vovova1111111111111111111111" },
    { OK,  "*vova",		"vovova" },
    { OK,  "a*a*a*a",		"aaaa" },
    { OK,  "vov?a*",		"vov2avovaa" },
    { OK,  "*v",		"v v" },
    { NOK, "vov?a*",		"dsadvov2a" },
    { OK,  "*v",		"vova \n\n\n\n\n\n\n\n\n\n\t\t\t\t\tt\t\t\t\t\tv" },
    { NOK, "v",		"vova v" },
    { OK,  "*v",		"vova fd\n\r\tv" },
    { OK,  "*v",		"vova fd\n\r\tv" },
    { OK,  "*v",		"vova v" },
    { OK,  "*v",		"vova v" },
    { OK,  "****?*****v*******",		"G v" },
    { NOK, "?vova",		"2333333333333 33 3 3 3 33 3 3 33 3 3 3 1vova" },
    { NOK, "?vo???va?",		"sddasdsa -vo123va1" },
    { NOK, "vov?a*",		"dsadvov2a" },
    { NOK, "vova*",		"dcscdscsdcsdcdscdscvovacdscsdcdcddcsdc" },
    { OK,  "???*vova*",		"dddvova" },
    { OK,  "*vo?va",		"vowva" },
    { NOK, "*vova",		"vova5"},
    { NOK, "*vova",		"dcscdvova3" },
    { NOK, "*vova",		"vovadsadad3" },
    { NOK, "*vova",		"vo2va3" },
    { NOK, "*vova",		"v2ova3" },
    { NOK, "*vova",		"vova3" },
    { OK,  "*vo?va",		"fdsfe33 ef ewfwef wefwe fwe fwe fwef ewd wed wed2 d2 3dwe dw ed we vowva" },
    { NOK, "*vo????va",		"fdsfe33 ef ewfwef wefwe fwe fwe fwef ewd wed wed2 d2 3dwe dw ed we vowvA" },
    { NOK, "*vo?va",		"fdsfe33 ef ewfwef wefwe fwe fwe fwef ewd wed wed2 d2 3dwe dw ed we vowvsa" },
    { NOK, "*vo?va",		"fdsfe33 ef ewfwef wefwe fwe fwe fwef ewd wed wed2 d2 3dwe dw ed we vowva cdsc" },
    { OK,  "*vo?va",		"vowva" },
    { OK,  "vov?a*",		"vov2a" },
    { OK,  "vov?a*",		"vov2adsadsad" },
    { NOK, "vova*",		"dcscdscsdcsdcdscdscvocvacdscsdcdcddcsdc" },
    { NOK, "vova*",		"dcscdscsdcsdcdscdscvocvacdscsdcdcddcsdc" },
    { NOK, "vova*",		"dcscdscsdcsdcdscdscvocvacdscsdcdcddcsdc" },
    { NOK, "vova*",		"dcscdscsdcsdcdscdscvocvacdscsdcdcddcsdcvovadcsdcdscdsc" },
    { OK,  "???*vova*",		"dddvova" },
    { OK,  "???*vova*",		"vovvovadcdscsdc" },
    { NOK, "*vova*",		"Z" },
    { OK,  "*vova*",		"vovavova" },
    { OK,  "*vova*",		"vova" },
    { OK,  "*vova?",		"vova1" },
    { OK,  "*vova?",		"dsadvova1" },
    { NOK, "*vova?",		"dsadsaddsvova" },
    { OK,  "*vova?",		"eeeeeeee eeeevova2" },
    { NOK, "*vova?",		"dewdvovaee" },
    { NOK, "?vova*",		"wvovvovvovvova" },
    { OK,  "?vova*",		"wvovavovvovvova" },
    { OK,  "???vo??va*****",		"ddvvoxxvadccd" },
    { OK,  "?vova*",		"2vova" },
    { OK,  "?vova*",		"2vovadd" },
    { OK,  "?vova*",		"3vovaccsscadsc dcsacasdcasdc" },
    { OK,  "?vo?va?",		"cvodvad" },
    { OK,  "??vo??va??",		"cdvoddvadd" },
    { OK,  "???vo???va???",		"dddvosssvasss" },
    { OK,  "*?*vova",		"eeeee4eeeeevova" },
    { OK,  "*?*vova",		"4eeeeevova" },
    { OK,  "*?*vova",		"4vova" },
    { NOK, "*?*vova",		"vova" },
    { OK,  "vova*?*",		"vova3eeeee" },
    { OK,  "vova*?*",		"vovaeeeee3" },
    { OK,  "vova*?*",		"vova3" },
    { NOK, "vova*?*",		"vova" },
    { OK,  "*?*vova*?*",		"2vova2" },
    { OK,  "*?*vova*?*",		"eee2eeevovaeee2eee" },
    { OK,  "*?*vova*?*",		"eeee2vova2" },
    { OK,  "*?*vova*?*",		"2vova2eeeeee" },
    { OK,  "*?*vova*?*",		"2eeeeeevovaeeeeee2" },
    { NOK, "*?*vova*?*",		"vova" },
    { NOK, "*?*vova*?*",		"vova2" },
    { NOK, "*?*vova*?*",		"2vova" },
    { NOK, "*?*vova*?*",		"eeee2vova" },
    { NOK, "*?*vova*?*",		"2eeeeevova" },
    { NOK, "*?*vova*?*",		"eeee2eeeevova" },
    { NOK, "*?*vova*?*",		"vova2" },
    { NOK, "*?*vova*?*",		"vovaeee2" },
    { NOK, "*?*vova*?*",		"vovaeee2eeee" },
    { OK,  "*vo?va",		"vowva" },
    { NOK, "???*vova*",		"ddvova" },
    { NOK, "vova*",		"dcscdscsdcsdcdscdscvocvacdscsdcdcddcsdcvovadcsdcdscdsc" },
    { OK,  "*vova*",		"vova" },
    { OK,  "?vova*",		"2vova" },
    { OK,  "vova*?*",		"vova3" },
    { OK,  "*?*vova*?*",		"2vova2" },
    { OK,  "*?*vova*?*",		"eeee2vova2" },
    { OK,  "???*vova*",		"dddvova" },
    { OK,  "vova",		"vova" },
    { OK,  "*vova",		"vova" },
    { OK,  "v*ova",		"vova" },
    { OK,  "vo*va",		"vova" },
    { OK,  "vov*a",		"vova" },
    { OK,  "vova*",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "*****vova",		"vova" },
    { OK,  "v****ova",		"vova" },
    { OK,  "vo*************va",		"vova" },
    { OK,  "vov******************a",		"vova" },
    { OK,  "vova*************************",		"vova" },
    { OK,  "?vova",		"1vova" },
    { OK,  "v?ova",		"v1ova" },
    { OK,  "vo?va",		"vo1va" },
    { OK,  "vov?a",		"vov1a" },
    { OK,  "vova?",		"vova1" },
    { OK,  "??vova",		"11vova" },
    { OK,  "v??ova",		"v11ova" },
    { OK,  "vo??va",		"vo11va" },
    { OK,  "vov??a",		"vov11a" },
    { OK,  "vova??",		"vova11" },
    { OK,  "*vova",		"bla bla bla bla bla bla 11vova" },
    { OK,  "*vova",		"bla bla bla bla bla bla 1vova" },
    { OK,  "*vova",		"bla bla bla bla bla blavova" },
    { OK,  "*vova",		"bla bla bla bla bla blavova" },
    { OK,  "*vova",		"bla bla bla bla bla blavova" },
    { OK,  "*vova",		"bla bla bla bla blablavova" },
    { OK,  "*vova",		"bla bla bla bla bla bla-vova" },
    { NOK, "vova*",		"dewdewdew vovabla bla bla bla bla 1 111vova" },
    { NOK, "vova*",		"dewdewdew vova1 bla bla bla bla bla 1 111vova" },
    { NOK, "vova*",		"dewdewdew vova-bla bla bla bla bla 1 111vova" },
    { NOK, "vova*",		"dewdewdew vovabla bla bla bla bla 1 111vova" },
    { NOK, "vova*",		"dewdewdew vovabla bla bla bla bla 1 111vova" },
    { NOK, "vova*",		"dewdewdew vova2bla bla bla bla bla 1 111vova" },
    { NOK, "vova*",		"dewdewdew vova bla bla bla bla bla 1 111vova" },
    { NOK, "?vova",		"2333333333333 33 3 3 3 33 3 3 33 3 3 3 1vova" },
    { NOK, "?vova",		"2333333333333 33 3 3 3 33 3 3 33 3 3 3 1vova" },
    { NOK, "?vova",		"2333333333333 33 3 3 3 33 3 3 33 3 3 3 1vova" },
    { NOK, "?vova?",		"sdads sd sad sad sad sad sad sad sad sa ds 1vova1" },
    { NOK, "?vova?",		"sdads sd sad sad sad sad sad sad sad sa ds 1vovaa" },
    { NOK, "?vova?",		"sdads sd sad sad sad sad sad sad sad sa ds 1vovav" },
    { NOK, "?vova?",		"sdads sd sad sad sad sad sad sad sad sa ds 1vovav" },
    { NOK, "?vo???va?",		"sddasdsa -vo123va1" },
    { NOK, "?vo???va?",		"sddasdsa -vo123va1" },
    { NOK, "?vo???va?",		"sddasdsa -vo123va1" },
    { NOK, "?vo???va?",		"sddasdsa -vo123va1" },
    { NOK, "?v*o???va?",		"dssads 1vEEEEo333va1" },
    { NOK, "?v*o???va?",		"dssads 1vo333va1" },
    { NOK, "?v*o???va?",		"dssads 1vEEEEo333va1" },
    { NOK, "?v*o???va?",		"dssads 1vo333va1" },
    { NOK, "?v****o???va?",		"sdsad 1vo333va1" },
    { NOK, "?v****o???va?",		"sdsad 1vo333va1" },
    { NOK, "?v****o???va?",		"sdsad 1vo333va1" },
    { NOK, "?v****o???v*a?",		"dcfds 1vo333va1" },
    { NOK, "?v****o???v*a?",		"dcfds 1vo333va1" },
    { NOK, "?v****o???v*a?",		"dcfds 1vo333va1" },
    { NOK, "?v****o???v*a?",		"dcfds 1vo333va1" },
    { NOK, "?v****o???v***a?",		"dfdfdsf 1vo333vEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEa1" },
    { NOK, "?v****o???v***a?",		"dfdfdsf 1vo333vEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEa1" },
    { NOK, "?v****o???v***a?",		"dfdfdsf 1vo333vEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEa1" },
    { NOK, "?v****o???v***a?",		"dfdfdsf 1vo333vEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEa1" },
    { NOK, "?v****o???v***a?",		"dfdfdsf 1vo333va1" },
    { NOK, "?v****o???v***a?",		"dfdfdsf 1vo333va1" },
    { OK,  "vova",		"vova" },
    { OK,  "*vova",		"vova" },
    { OK,  "v*ova",		"vova" },
    { OK,  "vo*va",		"vova" },
    { OK,  "vov*a",		"vova" },
    { OK,  "vova*",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "*****vova",		"vova" },
    { OK,  "v****ova",		"vova" },
    { OK,  "vo*************va",		"vova" },
    { OK,  "vov******************a",		"vova" },
    { OK,  "vova*************************",		"vova" },
    { OK,  "?vova",		"1vova" },
    { OK,  "v?ova",		"v1ova" },
    { OK,  "vo?va",		"vo1va" },
    { OK,  "vov?a",		"vov1a" },
    { OK,  "vova?",		"vova1" },
    { OK,  "??vova",		"11vova" },
    { OK,  "v??ova",		"v11ova" },
    { OK,  "vo??va",		"vo11va" },
    { OK,  "vov??a",		"vov11a" },
    { OK,  "vova??",		"vova11" },
    { OK,  "*vova",		"bla bla bla bla bla bla 11vova" },
    { OK,  "*vova",		"bla bla bla bla bla bla 1vova" },
    { OK,  "*vova",		"bla bla bla bla bla blavova" },
    { OK,  "*vova",		"bla bla bla bla bla blavova" },
    { OK,  "*vova",		"bla bla bla bla bla blavova" },
    { OK,  "*vova",		"bla bla bla bla blablavova" },
    { OK,  "*vova",		"bla bla bla bla bla bla-vova" },
    { OK,  "vova*",		"vovabla bla bla bla bla 1 111vova" },
    { OK,  "vova*",		"vova1 bla bla bla bla bla 1 111vova" },
    { OK,  "vova*",		"vova-bla bla bla bla bla 1 111vova" },
    { OK,  "vova*",		"vovabla bla bla bla bla 1 111vova" },
    { OK,  "vova*",		"vovabla bla bla bla bla 1 111vova" },
    { OK,  "vova*",		"vova2bla bla bla bla bla 1 111vova" },
    { OK,  "vova*",		"vova bla bla bla bla bla 1 111vova" },
    { OK,  "?vova",		"1vova" },
    { OK,  "?vova",		"1vova" },
    { OK,  "?vova",		"1vova" },
    { OK,  "?vova?",		"1vova1" },
    { OK,  "?vova?",		"1vovaa" },
    { OK,  "?vova?",		"1vovav" },
    { OK,  "?vova?",		"1vovav" },
    { OK,  "?vo???va?",		"-vo123va1" },
    { OK,  "?vo???va?",		"-vo123va1" },
    { OK,  "?vo???va?",		"-vo123va1" },
    { OK,  "?vo???va?",		"-vo123va1" },
    { OK,  "?v*o???va?",		"1vEEEEo333va1" },
    { OK,  "?v*o???va?",		"1vo333va1" },
    { OK,  "?v*o???va?",		"1vEEEEo333va1" },
    { OK,  "?v*o???va?",		"1vo333va1" },
    { OK,  "?v****o???va?",		"1vo333va1" },
    { OK,  "?v****o???va?",		"1vo333va1" },
    { OK,  "?v****o???va?",		"1vo333va1" },
    { OK,  "?v****o???v*a?",		"1vo333va1" },
    { OK,  "?v****o???v*a?",		"1vo333va1" },
    { OK,  "?v****o???v*a?",		"1vo333va1" },
    { OK,  "?v****o???v*a?",		"1vo333va1" },
    { OK,  "?v****o???v***a?",		"1vo333vEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEa1" },
    { OK,  "?v****o???v***a?",		"1vo333vEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEa1" },
    { OK,  "?v****o???v***a?",		"1vo333vEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEa1" },
    { OK,  "?v****o???v***a?",		"1vo333vEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEa1" },
    { OK,  "?v****o???v***a?",		"1vo333va1" },
    { OK,  "?v****o???v***a?",		"1vo333va1" },
    { NOK, "vova",		"vo1va" },
    { NOK, "vova",		"vo1va" },
    { NOK, "vova",		"vo1va" },
    { NOK, "vova",		"vo1va" },
    { NOK, "vova",		"vo1va" },
    { NOK, "vova",		"vo1va" },
    { NOK, "vova",		"vo1va" },
    { OK,  "v?*?*?*?*ova",		"v4444ova" },
    { OK,  "v?*?*?*?*ova",		"v666666ova" },
    { OK,  "v?*?*?*?*ova",		"vwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwova" },
    { NOK, "v?*?*?*?*ova",		"v333ova" },
    { NOK, "v?*?*?*?*ova",		"v33ova" },
    { NOK, "v?*?*?*?*ova",		"v3ova" },
    { NOK, "v?*?*?*?*ova",		"vova" },
    { NOK, "v?*?*?*?*ova",		"v333ova" },
    { OK,  "v?*?*?*?*ova",		"v????ova" },
    { OK,  "v?*?*?*?*ova",		"v((((ova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "v",		"vfddfsdfdsf\nv" },
    { OK,  "*",		"vfddfsdfdsf\nv" },
    { NOK, "a",		"\n" },
    { NOK, "a",		"\n" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { OK,  "vova",		"vova" },
    { NOK, "v",		"1 2 2 3456 7 8 54t gdfgf v2 3 vova 1vv sddssdcds cddsd" },
    { NOK, "v",		"1 2 2 3456 7 8 54t gdfgf v2 3 vova 1vv sddssdcds cddsd" },
    { NOK, "v",		"1 2 2 3456 7 8 54t gdfgf v2 3 vova 1vv sddssdcds cddsd" },
    { NOK, "v",		"1 2 2 3456 7 8 54t gdfgf v2 3 vova 1vv sddssdcds cddsd" },
    { NOK, "v",		"1 2 2 3456 7 8 54t gdfgf v2 3 vova 1vv sddssdcds cddsd" },
    { NOK, "v",		"1 2 2 3456 7 8 54t gdfgf v2 3 vova 1vv sddssdcds cddsd" },
    { NOK, "v",		"1 2 2 3456 7 8 54t gdfgf v2 3 vova 1vv sddssdcds cddsd" },
    { NOK, "v",		"1 2 2 3456 7 8 54t gdfgf v2 3 vova 1vv sddssdcds cddsd" },
    { NOK, "v",		"1 2 2 3456 7 8 54t gdfgf v2 3 vova 1vv sddssdcds cddsd" },
    { NOK, "vova",		"vov\nvov \n vovvvv \n \n \t\nvova \n" },
    { OK,  "vova",		"vov\nvov \n vovvvv \n \n \t\nvova \nvova" },
    { NOK, "vova?",		"vov\nvov \n vovvvv \n \n \t\nvova  \nvova" },
    { OK,  "vova?",		"vov\nvov \n vovvvv \n \n \t\nvova \nvova1" },
    { OK,  "vova????",		"vov\nvov \n vovvvv \n \n \t\nvova \nvova4444" },
    { OK,  "vova*",		"vov\nvov \n vovvvv \n \n \t\nvova \nvova4444" },
    { OK,  "vova*",		"vov\nvov \n vovvvv \n \n\t \nvova \nvova4444 dew de dew d ewd ewd \n ddscdsc" },
    { NOK, "vova?",		"vov\nvov \n vovvvv \n \n\t \nvova  \nvova4444 dew de dew d ewd ewd \n ddscdsc" },
    { NOK, "a",		"vova \n vfdv \n fvfd \n cdsc \t v \vvfdv " },
    { NOK, "a",		"vova \n vfdv \n fvfd \n cdsc \t v \vvfdv \na " },
    { OK,  "a",		"vova \n vfdv \n fvfd \n cdsc \t v \vvfdv \na" },
    { OK,  "vova",		"vo><?><!@#$%^&*()_+~|?\nvova" },
    { NOK, "vova",		"vo><?><!@#$%^&*()_+~|?\nvova." },
    { NOK, "vova",		"vova\t" },
};


void TestKMP::runTestKMP()
{
    printf("\n----Core test----\n");
    KMP kmp;
    ResultPosition result = { 0 };
    
    char* text = NULL;
    char* mask = NULL;
    bool isFail = false;
    size_t sizeStr = 0;

    size_t countTests = sizeof(arrayTest) / sizeof(*arrayTest);
    size_t countFailed = 0;

#ifdef RUN_TIME_TEST
    std::map<clock_t, size_t> resTimeMap;
    clock_t workTime = clock();
    ptrdiff_t allTime = 0;
#endif
    
    for (size_t i = 0; i < countTests; ++i)
    {
        // getting the i-th mask
        mask = (char*)arrayTest[i][1];
        text = (char*)arrayTest[i][2];
        isFail = (arrayTest[i][0] == NOK);
        
        if (!kmp.setFilter(mask))
            continue;
        sizeStr = strlen(arrayTest[i][2]);

#ifdef RUN_TIME_TEST
        // calculation of the average time of the i-th test
        workTime = clock();
        for (size_t j = 0; j < REPEAR_TEST_FOR_MEASURE_TIME; ++j)
        {
            result = kmp.GetNextLineKMP(text, sizeStr);
            if (result.type == EPIC_FAIL)
                break;
            kmp.resetSearchState(); // I reset the state tk the string did not end, otherwise the algorithm will continue to search
        }
        workTime = clock() - workTime;


        // save time
        resTimeMap[workTime] = i + 1;
        allTime += workTime;
#endif

        // calculating the success of the i-th test
        result = kmp.GetNextLineKMP(text, sizeStr);
        kmp.resetSearchState();
        if (result.type == EPIC_FAIL)
            continue;

        if (isFail)
        {
            if (!(result.type == FAIL || result.type == STILL_SUCCESS_WAIT_NEXT_BLOCK))
            {
                fprintf(stderr, "[%zu/%zu]: mask '%s' matched '%s' unexpectedly. \n\n", (i + 1), countTests, mask, text);
                ++countFailed;
            }
        }
        else
        {
            if (!(result.type == SUCCESS))
            {
                fprintf(stderr, "[%zu/%zu]: mask '%s' didn't match '%s' as expected. \n\n", (i + 1), countTests, mask, text);
                ++countFailed;
            }
        }
    }

    printf("%llu/%llu tests succeeded\n", countTests - countFailed, countTests);

#ifdef RUN_TIME_TEST
    // I derive statistics on time
    size_t i = 0;
    printf("\nTime: 10 big times\n");
    for (std::map<clock_t, size_t>::reverse_iterator it = resTimeMap.rbegin(); it != resTimeMap.rend() && i < 10; ++it, ++i)
        printf("test #%zu time = %f ms\n", it->second, (double)it->first / (double)REPEAR_TEST_FOR_MEASURE_TIME);

    double mediumTime = (double)allTime / (double)countTests / (double)REPEAR_TEST_FOR_MEASURE_TIME;
    printf("\nAll time = %zu ms. Medium time = %f ms\n", allTime, mediumTime);
#endif
    
}

#endif