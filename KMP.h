#pragma once
#ifndef KMP_H
#define KMP_H
#include "Core.h"

class KMP
{
public:
    KMP();
    ~KMP();
    /*
        The function takes a string and size.
        Searches for a string by mask, where:
             '*' - 0 or more arbitrary characters
             '?' - 1 arbitrary character
        Returns one of the options:
             - success + shift to the desired line
             - possible success + shift to possibly successful line (in this case, the search saves the state)
             - confirmed success of the previous line
             - fail
             - mistake
    */
    ResultPosition GetNextLineKMP(const char* str, size_t strSize);
    
    /*
        The function takes an input string - the desired mask. Generates a table of prefixes according to the KMP algorithm.
        If the size of the mask does not exceed a certain value, then the heap memory is not allocated for the prefix table.
        The prefix table is formed taking into account that:
            '*' - 0 or more arbitrary characters
            '?' - 1 arbitrary character
    */
    bool setFilter(const char* mask);

    /*
        The function resets the "search state".
    */
    void resetSearchState();

private:
    // prefix generation using the Knut - Morris - Pratt algorithm.
    inline void createKMPPrefix();
    inline void createKMPPrefixImpl(size_t l, size_t r);

    // KMP algorithm
    inline void badCurStrShiftToNewLine();
    inline bool checkEndLine();
    inline void shiftToLeftPrefixKMP();
    inline ResultPosition returnSuccess();
    inline ResultPosition returnFail();

private:
    const char* mMask;
    size_t mMaskSize;

    const char* mStr;
    size_t mStrSize;
    
    size_t mStrIndex;
    size_t mMaskIndex;
    size_t mFoundRowIndex;

    bool mContinueSearch;
    bool mAfterStar;    
    bool mFail;
    ResultPosition mResult;
    
    // size_t tk value can be equal to the maximum index of the mask array
    size_t* mPrefixKMP;
    size_t mPrefixKMPSSO[MASK_SSO_SIZE]; // Small String Optimization for PrefixKMP
};

#endif
