#include "Core.h"
#include "KMP.h"
#include "CLogReader.h"

#ifdef RUN_TEST
#include "TestKMP.h"
#include "TestCLogReader.h"
#endif

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        ERR("Enter parameters. Example: LogReader.exe \"test.txt\" \"*Hello World?\"");
        return 0;
    }

    const char* filename = argv[1];
    const char* mask = argv[2];
     
    if (!filename || !mask)
    {
        ERR("Enter parameters. Example: LogReader.exe \"test.txt\" \"*Hello World?\"");
        return 0;
    }

#ifdef RUN_TEST
    TestKMP::runTestKMP();
    TestCLogReader::runTestFileBlockSize(filename, mask);
    TestCLogReader::runTestResultBlockSize(filename, mask);
    return 0;
#endif

    CLogReader logReader;
    if (!logReader.Open(filename))
    {
        ERR("Cannot open file\n");
        return 0;
    }

    if (!logReader.SetFilter(mask))
    {
        ERR("Cannot set filter");
        return 0;
    }

    char* foundString = new char[FOUND_STRING_SIZE];
    
    while (logReader.GetNextLine(foundString, FOUND_STRING_SIZE))
        printf("%s\n", foundString);
    
    logReader.Close();
    return 0;
}
