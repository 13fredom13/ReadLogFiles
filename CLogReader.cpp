#include "CLogReader.h"
#pragma warning(disable : 4996)

CLogReader::CLogReader(size_t fileBufferSize) : 
    mFile(NULL), 
    mFileBuffer(NULL), 
    mBuffer(NULL), 
    mFileBufferSize(fileBufferSize), 
    mLeaveBufferSize(0), 
    mCurPtrFoundStr(NULL), 
    mCurFoundStrMaxSize(0)
{
    memset(mFileBufferSSO, 0, (READ_FILE_SSO_SIZE) * sizeof(char));
    mResult.number = 0;
    mResult.type = 0;

    // allocate memory if good size and manage to allocate
    if (mFileBufferSize >= READ_FILE_SSO_SIZE)
    {
        if (mFileBuffer = new (std::nothrow) char[mFileBufferSize])
        {
            mFileBufferSSO[USED_STACK] = 0; // not using SSO
            memset(mFileBuffer, 0, (mFileBufferSize) * sizeof(char));
        }
    }

    // If small size or highlight failed
    if (!mFileBuffer)
    {
        mFileBuffer = mFileBufferSSO + 1;
        mFileBufferSSO[USED_STACK] = 1; // used SSO
        mFileBufferSize = READ_FILE_SSO_SIZE - 1;
    }
}

CLogReader::~CLogReader()
{
    // if SSO was not used and the pointer is valid - free memory
    if (!mFileBufferSSO[USED_STACK] && mFileBuffer) 
    {
        delete[] mFileBuffer;
        mFileBuffer = NULL;
    }

    TryClose();
}

bool CLogReader::Open(const char* filename)
{
    if (!filename)
    {
        ERR("Missing file name");
        return false;
    }

    TryClose();

    if ((mFile = fopen(filename, "rb")))
        return true;

    return false;
}

void CLogReader::Close()
{
    TryClose();
}

void CLogReader::TryClose()
{
    // reset "search state"
    mKMP.resetSearchState();
    if (mFile)
        fclose(mFile);
}

bool CLogReader::SetFilter(const char* filter)
{
    if (!filter)
    {
        ERR("Missing filter");
        return false;
    }

    // save mask and create prefix table
    return mKMP.setFilter(filter);
}

bool CLogReader::GetNextLine(char* foundStr, const int foundStrMaxSize)
{
    if (!foundStr || foundStrMaxSize <= 0)
    {
        ERR("Missing buffer for response or size equals zero");
        return false;
    }

    // save buffer pointer and size for gradual filling
    mCurPtrFoundStr = foundStr;
    mCurFoundStrMaxSize = foundStrMaxSize;

    // check and skip UTF8 header
    if (!mLeaveBufferSize)
        FixUTF8Header();

    while (true)
    {
        // Checking the availability of data and loading from a file if necessary
        if (!CheckReadBuf())
            return false;

        // obtaining a search result and analysis
        mResult = mKMP.GetNextLineKMP(mBuffer, mLeaveBufferSize);
        switch (mResult.type)
        {
        case SUCCESS:
        {
            // update pointer and size, as the result could be partially recorded
            mCurPtrFoundStr = foundStr;
            mCurFoundStrMaxSize = foundStrMaxSize;

            CopyBufShiftPtr();

            const int lastIndex = (mCurFoundStrMaxSize == 0 ? foundStrMaxSize - 1 : foundStrMaxSize - (int)mCurFoundStrMaxSize);
            foundStr[lastIndex] = '\0';

            return true;
        }
        case FAIL:
        {
            assert(!mResult.number);

            // update pointer and size, as the result could be partially recorded
            mCurPtrFoundStr = foundStr;
            mCurFoundStrMaxSize = foundStrMaxSize;

            // the entire data block did not fit, zero the buffer except the last char, it can be equal to '\n' or '\r'
            mBuffer += (mLeaveBufferSize - 1);
            mLeaveBufferSize = 1;

            // called to move the pointer to the next line
            CopyBufShiftPtr();
            break;
        }
        case STILL_SUCCESS_WAIT_NEXT_BLOCK:
        {
            // if a new "possible success" has come, then you need to reset the buffer for the result
            if (mResult.number)
            {
                mCurPtrFoundStr = foundStr;
                mCurFoundStrMaxSize = foundStrMaxSize;
            }

            // call to move the pointer to the next line
            CopyBufShiftPtr();
            break;
        }
        case SUCCESS_PREVIOUS_ROW:
        {
            // The success of the previous line was confirmed. Add a buffer, move the pointer to the next line and exit
            assert(!mResult.number);
            CopyBufShiftPtr();

            const int lastIndex = (mCurFoundStrMaxSize == 0 ? foundStrMaxSize - 1 : foundStrMaxSize - (int)mCurFoundStrMaxSize);
            foundStr[lastIndex] = '\0';

            return true;
        }
        case EPIC_FAIL:
        {
            ERR("KMP returned an EPIC FAIL");
            return false;
        }
        default:
        {
            ERR("KMP returned an unexpected response");
            break;
        }
        }
    }
}

void CLogReader::CopyBufShiftPtr()
{
    // move the pointer to the beginning of the found line
    mBuffer += mResult.number;
    mLeaveBufferSize -= mResult.number;

    size_t shift = 0;
    size_t writeBytes = 0;

    while (true)
    {
        // find '\n' or '\r':
        shift = 0;
        while (shift < mLeaveBufferSize && (mBuffer[shift] != '\n' && mBuffer[shift] != '\r'))
            ++shift;

        // write data if not fail (or add)
        writeBytes = (mCurFoundStrMaxSize < shift ? mCurFoundStrMaxSize : shift);
        if (mResult.type != FAIL && writeBytes)
        {
            if (writeBytes > 1)
                memcpy(mCurPtrFoundStr, mBuffer, (writeBytes) * sizeof(char));

            // refresh pointer and size of recorded data
            mCurFoundStrMaxSize -= writeBytes;
            mCurPtrFoundStr += writeBytes;
        }

        // move to '\n' or '\r' or to the end of the data block
        mBuffer += shift;
        mLeaveBufferSize -= shift;

        // if this is a possible success, it is necessary to transfer the next block for analysis
        if (mResult.type == STILL_SUCCESS_WAIT_NEXT_BLOCK)
        {
            assert(!mLeaveBufferSize);
            return;
        }

        // if found '\n' or '\r'
        if (mLeaveBufferSize)
        {
            // shift to new char
            ShiftToNextChar();
            return;
        }
        else if (!CheckReadBuf()) // read the new block; if it could not read, exit
        {
            return;
        }
    }
}

// called if '\n' or '\r' is found but no char is found after '\n' or '\r'
void CLogReader::ShiftToNextChar()
{
    size_t shift = 0;
    while (true)
    {
        // find char after '\n' or '\r'
        shift = 0;
        while (shift < mLeaveBufferSize && (mBuffer[shift] == '\n' || mBuffer[shift] == '\r'))
            ++shift;

        mLeaveBufferSize -= shift;
        mBuffer += shift;

        // if char found, exit
        if (mLeaveBufferSize)
            return;

        // if you can�t find it, read the new block; if you couldn�t read it, exit
        if (!CheckReadBuf())
            return;
    }
}

bool CLogReader::CheckReadBuf()
{
    // if there is no data left and it was not possible to read new ones, exit
    if (!mLeaveBufferSize && !ReadNewBlock())
        return false;

    return true;
}

bool CLogReader::ReadNewBlock()
{
    // read new data block
    if (size_t readSize = fread(mFileBuffer, sizeof(char), mFileBufferSize, mFile))
    {
        assert(!mLeaveBufferSize);
        mBuffer = mFileBuffer;
        mLeaveBufferSize += readSize;
        return true;
    }

    // if you�ve finished working with the file, clear the 'search state'
    mKMP.resetSearchState();
    return false;
}

void CLogReader::FixUTF8Header()
{
    // read new data block
    if (size_t readSize = fread(mFileBuffer, sizeof(char), mFileBufferSize, mFile))
    {
        mBuffer = mFileBuffer;
        mLeaveBufferSize += readSize;
    }

    // check and skip UTF8 header
    if (mLeaveBufferSize >= 3 && mBuffer[0] == (char)0xEF && mBuffer[1] == (char)0xBB && mBuffer[2] == (char)0xBF)
    {
        mBuffer += 3;
        mLeaveBufferSize -= 3;
    }
}