#pragma once
#ifndef TEST_H
#define TEST_H

#include "Core.h"
#include "KMP.h"

#define OK    ((char*) 1)
#define NOK   ((char*) 0)

class TestKMP
{
public:
    /*
        The function tests the search engine without reading the file.
        The function starts the search on N lines and compares the results with the correct answer.
        Displays statistics on success and on time.
    */
    static void runTestKMP();
};

#endif
