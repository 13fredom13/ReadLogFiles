#pragma once
#ifndef CORE_H
#define CORE_H

#include <iostream>
#include <cstring>
#include <cassert>
#include <cstdio>
#include <cstddef>


const size_t READ_FILE_BLOCK_SIZE = 50000;
const int READ_FILE_SSO_SIZE = 128;
const int MASK_SSO_SIZE = 16;
const int FOUND_STRING_SIZE = 30;


#define ERR(x) fprintf(stderr, "ERROR: " x "\n")
//#define RUN_TEST


enum { STAR = '*', QUESTIONMARK = '?' };
enum { USED_STACK = 0 };
enum { FAIL, SUCCESS, STILL_SUCCESS_WAIT_NEXT_BLOCK, SUCCESS_PREVIOUS_ROW, EPIC_FAIL };


struct ResultPosition {
    char type;
    size_t number;
};

#endif