#pragma once
#ifndef CLOGREADER_H
#define CLOGREADER_H
#include "Core.h"
#include "KMP.h"

class CLogReader
{
public:
    CLogReader(size_t fileBufferSize = READ_FILE_BLOCK_SIZE);
    ~CLogReader();

    /*
        Attempts to close a file.
    */
    void Close();
    
    /*
        Open file
    */
    bool Open(const char* nameFile);

    /*
        Gets a pointer to a mask and passes it to the search engine.
    */
    bool SetFilter(const char* filter);
    
    /*
        Gets a pointer to data and maximum length. The last character will always be '\0'.
    */
    bool GetNextLine(char* foundStr, const int foundStrMaxSize);


private:
    CLogReader(const CLogReader&);
    CLogReader& operator=(const CLogReader&);

    /*
        Checking the availability of data and loading from a file if necessary
    */
    inline bool CheckReadBuf();

    /*
        Reading a data block from a file
    */
    inline bool ReadNewBlock();
    
    /*
        Saving data to the buffer and moving the pointer to the next line
    */
    inline void CopyBufShiftPtr();

    /*
        Auxiliary function to shift the pointer to the first character of the next line
    */
    inline void ShiftToNextChar();

    /*
        Skip EB BB BF char at the beginning of the file
    */
    inline void FixUTF8Header();

    /*
        If the file is open - closes
    */
    inline void TryClose();

private:
    
    KMP mKMP; // search core, Bridge pattern
    FILE* mFile;
    ResultPosition mResult;

    char* mFileBuffer;
    char* mBuffer;
    char  mFileBufferSSO[READ_FILE_SSO_SIZE]; // Small String Optimization for FileBuffer

    size_t mFileBufferSize;
    size_t mLeaveBufferSize;

    // The size that remains to be written to the buffer of the found line and the pointer by which to write
    char* mCurPtrFoundStr;
    size_t mCurFoundStrMaxSize;
};

#endif