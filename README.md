# **Legacy code**
# Поиск строк в файле по заданной маске.
Маска может включать спецсимволы:
* 	'*' - 0 или более произвольных символов
* 	'?' - 1 произвольный символ


*Код не использует сторонние библиотеки (в том числе STL) и компоненты.* <br/>
Единственное исключение составляют тесты, которые включаются с помощью директивы #RUN_TEST из Core.h <br/>
Непосредственно поиском "занимается" класс KMP, использует алгоритм Кнута — Морриса — Пратта для поиска строк.

Класс CLogReader:
* Читает блоки данных из файлов UTF8(*для 1 байтовых char*) или ANSI.
* Обрабатывает файлы, состоящие из нескольких частей, разделенные между собой через EOF (такие, как некоторые архивы). 
* Обрабатывает различный перенос строк LF, CR, CR+LF,  

После получения блока данных, алгоритм их передает для поиска классу KMP.<br/>
Использует Small String Optimization для символьного массива.


# Алгоритм Кнута — Морриса — Пратта
Производит поиск строки по маске, где:
* 	'*' - 0 или более произвольных символов
* 	'?' - 1 произвольный символ


Получает маску, строит таблицу префиксов с учетом '\*' и '?' <br/>
Подряд идущие '*' учитывает как одну. <br/>
Если размер маски небольшой, тогда таблицу префиксов помещает в статический массив. Иначе, выделяет память в куче. <br/>
На вход получает строку и размер строки. <br/>
Возвращает один из вариантов:
* 	успех + сдвиг к искомой строке
* 	неуспех
* 	возможный успех + сдвиг к возможно успешной строке
* 	подтвержденный успех предыдущей строки - если возможный успех подтвердился
* 	ошибка

Если функция возвращает "возможный успех", тогда она сохраняет "состояние поиска" для того, чтобы продолжить поиск. 
В остальных случаях "состояние поиска" сбрасывается и на вход ожидается новая строка. 
Под "состоянием поиска" понимается количество найденных символов в маске, а также другие величины. 
**Состояние поиска позволяет читать из файла блоки текста, не модифицируя сам блок данных (например, не сдвигая указатель на конец строки).**
Тем самым можно производить поиск в больших файлах, состоящих из одной или нескольких строк.

Алгоритм выполняется за O(N + M), где N - размер данных, M - размер маски. <br/>
Алгоритм использует M дополнительной памяти. <br/>
Алгоритм использует Small String Optimization - не выделяет блок в куче, если размер маски меньше определенной величины. <br/>

# Тест

*  Реализован тест для поискового ядра - KMP класса, без чтения из файла.
*  Тест всего проекта: из файла читаются блоков различных размеров, сравниваются результаты и время. 
*  Тест всего проекта: результаты записываются в блоки разных размеров. Сравниваются результаты и время.

# Интерфейс
class CLogReader <br/>
{ <br/>
public: <br/>
    CLogReader(size_t fileBufferSize); <br/>
    ~CLogReader(); <br/>
    bool Open(const char*); // открытие файла, false - ошибка <br/>
    void Close();   // закрытие файла <br/>
    bool SetFilter(const char *filter); // установка фильтра строк, false - ошибка <br/>
    bool GetNextLine(char *buf, const int bufsize); // запрос очередной найденной строки, buf - буфер, bufsize - максимальная длина. false - конец файла или ошибка <br/>
}; <br/>
